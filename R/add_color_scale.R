#' Add a continuous color scale to an echart
#'
#' Creates a mapping from `[min, max]` of the given dimension to the `colors`
#'
#' @param echart echart
#' @param dimension Which data dimension to map to colors. Default to the last
#'   column.
#' @param min Min data value. Must be specified. The dimension's min is a good
#'   choice.
#' @param max Max data value. Must be specified. The dimension's max is a good
#'   choice.
#' @param colors Character vector of colors. The color palette will be a
#'   gradient between those.
#' @param show_legend Whether to show the legend.
#' @param format Format of the legend as a string. Use `{value}` as value
#'   placeholder.
#' @param orientation Orientation of the legend. One of "vertical" or
#'   "horizontal".
#' @param interactive If TRUE the legend has handles that can be used to filter
#'   the data dynamically.
#' @param labels Character vector of length 2 containing the legend labels, e.g.
#'   `c("high", "low")`. Default = NULL.
#' @param inverse If TRUE, the color palette is reversed.
#' @param text_style Text style for the legend. See `make_textstyle`.
#' @param box_style Box style for the legend, See `make_boxstyle`.
#' @param ... Tidy dots passed as extra visualMap parameters. See details.
#'
#'
#' @details The visual map and legend can be customised further by passing extra
#'   parameters to `...`.
#'
#'   To control the legend position: `top`, `right`, `bottom`, `left`, `padding` to
#'   control the position (default is `left = bottom = 0` and `top = right = "auto"`
#'   and `padding = 5`).
#'
#'   To control the legend size: `itemWidth` (default = 20) and `itemHeight` (default = 140)
#'
#'  To control the handle position: `align` (one of "auto", "left", "right", )
#'
#'   See https://ecomfe.github.io/echarts-doc/public/en/option.html#visualMap-continuous.type
#'   for the full list of available visualMap parameters.
#'
#' @export
add_color_scale <- function(echart,
                            dimension,
                            min, max, colors,
                            show_legend = TRUE,
                            format = NULL,
                            orientation = c("vertical", "horizontal"),
                            interactive = FALSE,
                            labels = NULL,
                            inverse = FALSE,
                            text_style = list(),
                            box_style = list(),
                            ...) {

  visualMap = rlang::list2(type = "continuous",
                           min = min,
                           max = max,
                           calculable = interactive,
                           show = show_legend,
                           textStyle = text_style,
                           formatter = format,
                           inverse = inverse,
                           orient = match.arg(orientation, c("vertical", "horizontal")),
                           !!! box_style,
                           text = labels,
                           ...)

  inRange = list(
    color = colors
  )

  out <- add_options(echart, list(visualMap = visualMap), unbox = TRUE)
  # Adding colors separately because they must not be unboxed.
  add_options(out, list(visualMap = list(inRange = inRange)), unbox = FALSE)
}