#' Plot an ECharts gauge
#'
#' @param echart Base echart (as returned by `echart()`)
#' @param value Value of the gauge, numeric.
#' @param title Title for the plot (used as series name)
#' @param range Min and max of the plot range.
#' @param radius How large the gauge is. Default to "95\%".
#' @param angles Start and end angle (in degrees, -360 to 360 are valid values) for the gauge background.
#'   Default to `c(225, -45)`
#' @param clockwise Whether the gauge is oriented clockwise. Default to TRUE.
#' @param saveAsImage File name under which to save the gauge.
#'   Set to `NULL` to disable saving. Default to "gauge" which saves to "gauge.png".
#' @param ... Extra options (passed to `add_options`)
#' @param value_format Format of the value being printed.
#'   Use `{value}` for the gauge value.
#' @param tooltip_text Tooltip prefix. Default to title.
#' @param tooltip_format Format of the tooltip.
#'   Use `{a}` for tooltip_text, `{b}` for title and `{c}` for gauge value.
#' @param animation TRUE (default) to enable animations.
#'
#' @importFrom rlang list2
#' @export
gaugeChart <- function(echart,
                       value,
                       title = "",
                       range = c(0, 100),
                       radius = "95%",
                       angles = c(225, -45),
                       clockwise = TRUE,
                       saveAsImage = NULL,
                       value_format = "{value}%",
                       tooltip_text = title,
                       tooltip_format = "{a}: {c}%",
                       animation = TRUE,
                       ...) {
  options <- list(
    series = list(
      type = unbox("gauge"),
      name = unbox(tooltip_text),
      data = data.frame(value = value, name = title),
      detail = list(formatter = unbox(value_format)),
      radius = unbox(radius),
      startAngle = unbox(angles[1]),
      endAngle = unbox(angles[2]),
      clockwise = unbox(clockwise),
      min = unbox(range[1]),
      max = unbox(range[2]),
      animation = unbox(animation)
    ),
    tooltip = list(formatter = unbox(tooltip_format))
  )

  if (!is.null(saveAsImage)) {
    options$toolbox <- list(
      feature = list(
        saveAsImage = list(
          type = "png",
          name = saveAsImage
        )
      )
    )
  }

  out <- add_options(echart, options)

  dots <- rlang::list2(...)
  if (length(dots) > 0) {
    out <- add_options(out, dots)
  }

  return(out)
}


#' Style a gauge plot
#'
#' This is a convenience function which builds the relevant list of echarts
#' options. More advanced customisation (e.g. shadows) can be achieved by
#' building the option list manually (following the echarts documentation) and
#' calling `add_options`.
#'
#' @param echart Gauge echart to style
#' @param width Width of the gauge axis line in pixel. Absolute only, not relative.
#' @param colors Array of colors (named or hex), same length as `color_breaks`
#' @param color_breaks Array of increasing breaks between 0 and 1.
#' @param show Named list of logicals setting visibitily.
#'   Names can contain `axisLine`, `splitLine`, `axisTick`, `axisLabel`.
#' @param ticks Number of ticks between each split. Default = 5.
#' @param tick_length Length of the ticks. Can be relative (e.g. "5\%").
#' @param tick_linestyle Linestyle for the tick. See `?make_linestyle`.
#' @param split_length Length of the split. Can be relative (e.g. "20\%").
#' @param split_linestyle Linestyle for the splir. See `?make_linestyle`
#' @param label_format Format of the labels. Use `{value}` to access the gauge value.
#' @param opacity Between 0 and 1, default = 1.
#'
#' @details This function sets the relevant axisLine, splitLine, axisTick and axisLabel options.
#'   Refer to the echarts documentation for more details.
#'
#' @importFrom purrr map2
#' @export
add_gaugestyle <- function(echart,
                           colors = c("#91c7ae", "#63869e", "#c23531"),
                           color_breaks = c(0.2, 0.8, 1),
                           width = 30,
                           ticks = 5,
                           tick_length = 8,
                           tick_linestyle = NULL,
                           split_length = 15,
                           split_linestyle = NULL,
                           opacity = 1,
                           show = list(
                             axisLine = TRUE,
                             splitLine = TRUE,
                             axisTick = TRUE,
                             axisLabel = TRUE
                           ),
                           label_format = "{value}") {
  color_tuple <- map2(color_breaks, colors, function(b, c) c(b, c))

  options <- list(
    axisLine = list(
      show = show$axisLine,
      lineStyle = list(
        color = color_tuple,
        width = width
      )
    ),
    splitLine = list(
      show = show$splitLine,
      length = split_length,
      lineStyle = split_linestyle
    ),
    axisTick = list(
      show = show$axisTick,
      splitNumber = ticks,
      length = tick_length,
      lineStyle = tick_linestyle
    ),
    axisLabel = list(
      show = show$axisLabel,
      formatter = label_format
    )
  )

  add_options(echart, list(series = options), unbox = TRUE)
}

#' Style the pointer of a gauge plot
#'
#' @param echart Gauge echart to style
#' @param length Length of the pointer. Can be a percentage of the radius.
#' @param width Width of the pointer.
#' @param color Color name or hex code. Set to "auto" (default) for adaptive colors.
#' @param show Set to TRUE (default) to show the pointer.
#' @param border Linestyle of the border.
#' @param opacity 0 to 1. Default = 1.
#'
#' @export
add_pointerstyle <- function(echart,
                             length = "80%", width = 8,
                             color = "auto", show = T,
                             border = make_linestyle(width = 0, color = "#000", type = "solid"),
                             opacity = 1) {
  options <- list(
    pointer = list(
      show = show,
      length = length,
      width = width
    ),
    itemStyle = list(
      color = color,
      borderColor = border$color,
      borderWidth = border$width,
      borderType = border$type,
      opacity = opacity
    )
  )

  add_options(echart, list(series = options), unbox = TRUE)
}

#' Style the detail/label of a gauge chart
#'
#' @param echart Gauge chart
#' @param show TRUE/FALSE
#' @param color Hex or name. "auto" (default) to use the current gauge color.
#' @param background_color Hex or name. "auto" to use the current gauge color.
#' @param border_width 0 for no border.
#' @param border_color Hex or name. "auto" to use the current gauge color.
#' @param offset (x, y) offset from center. Default = c(0, "40\%")
#' @param fontstyle A fontstyle as returned by `fontstyle`.
#'
#' @importFrom rlang list2 !!!
#' @export
add_detailstyle <- function(echart,
                            show = TRUE, background_color = "transparent",
                            border_width = 0, border_color = "transparent",
                            offset = c(0, "40%"),
                            color = "auto",
                            fontstyle = make_fontstyle(size = 15)) {
  options <- list(
    detail = rlang::list2(
      show = show,
      backgroundColor = background_color,
      borderColor = border_color,
      borderWidth = border_width,
      offsetCenter = offset,
      color = color,
      !!!fontstyle
    )
  )
  add_options(echart, list(series = options), unbox = TRUE)
}
