% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/radarChart.R
\name{radarControl}
\alias{radarControl}
\title{Advanced configuration for radar charts}
\usage{
radarControl(
  radius = "75\%",
  splitNumber = 5,
  shape = c("polygon", "circle"),
  fill = TRUE,
  fill.opacity = 0.2,
  scale = FALSE,
  scale.color = "#333",
  scale.size = 12,
  ...
)
}
\arguments{
\item{radius}{Radius of the chart. Default = "75\%".}

\item{splitNumber}{Number of splits. Default = 5.}

\item{shape}{Shape of the background lines. One of "polygon" (default) or "circle".}

\item{fill}{Whether to fill in the radar shapes. Default = FALSE.}

\item{fill.opacity}{Opacity of the fill if any. Default = 0.2.}

\item{scale}{Whether to show a vertical scale along the splits.}

\item{scale.color}{Color of the scale if shown.}

\item{scale.size}{Size of the scale}

\item{...}{Tidy dots for extra echarts arguments passed to the main radar option.}
}
\description{
Advanced configuration for radar charts
}
